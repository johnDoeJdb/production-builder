<?php

namespace ProductionBuilder;

define('BASE_DIR', __DIR__ . '/../../');

require_once BASE_DIR.'/vendor/autoload.php';

use ProductionBuilder\Command\PurgeExamplesCommand;
use ProductionBuilder\Command\PurgeTestsCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new PurgeTestsCommand());
$application->add(new PurgeExamplesCommand());
$application->run();
