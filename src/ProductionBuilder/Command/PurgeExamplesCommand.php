<?php

namespace ProductionBuilder\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class PurgeExamplesCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('production:examples:purge')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Provide path to folder'
            )
            ->setDescription('
                Delete all examples folder by pattern in selected folder
            ')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $counter = 0;
        $path = $input->getArgument('path');
        $finder = new Finder();
        $finder->directories()->in($path)->name('/([Ee]xamples)/');

        $folders = iterator_to_array($finder);
        foreach ($folders as $folder) {
            $folderPath = $folder->getRealPath();
            shell_exec('rm -R '.$folderPath);
            $counter++;
        }

        $output->writeln('Deleted examples folders: '.$counter);
    }
}